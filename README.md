# calliope

## Roadmap

- [ ] Query all items in playlist
- [ ] Query all playlists on account
- [ ] Login over OAuth2
- [ ] Playback using mpv
- [ ] Spotify support
- [ ] Search music
- [ ] Add item to playlist
- [ ] Remove item to playlist
- [ ] Seek during playback using hotkeys
- [ ] Progress bar
