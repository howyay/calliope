use libmpv::{events::*, *};

fn main() {
    println!("Hello, world!");
    let url = "https://music.youtube.com/watch?v=1GYR_a78lKo";
    let _ = play(&url);
}

// fn addToPlaylist(pl: &String) {
//     let rc = reqwest::Client::new();
//     let result = rc.post("https://www.youtube.com/youtubei/v1/browse/edit_playlist?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8");
// }

fn play(url: &str) -> Result<()>{
    let mpv = Mpv::new()?;
    mpv.command("loadfile", &[url])
}
